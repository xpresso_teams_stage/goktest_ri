"""xpresso Google Cloud Storage client"""

__author__ = 'Gagan'

import os
import json
import pandas as pd
from google.cloud import storage

from xpresso.ai.core.logging.xpr_log import XprLogger
import xpresso.ai.core.commons.utils.constants as constants
import xpresso.ai.core.commons.exceptions.xpr_exceptions as xpr_exp


class GoogleCloudStorageConnector:
    """
    This class is used to interact with Google Cloud Storage (GCS). GCS is
    highly durable `object` storage.
    """

    def __init__(self, user_config):
        """

        __init__() here initializes the XprLogger and client object. Sets environment
        variables as required by GCS. Takes a user-specified
        DSN from the config/common.json to set public project_id and corresponding
        dataset.

        """

        self.logger = XprLogger()
        self.client = None
        self.user_project_id = user_config.get(constants.project_id)
        self.dataset = user_config.get(constants.dataset)
        self.bucket_name = user_config.get(constants.dataset)
        path = user_config.get(constants.cred_path)
        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = path
        with open(path) as cred:
            self.cred_project_id = json.load(cred).get(constants.project_id)

        # TODO if required, remove later this implicit connect
        self.connect()

    def connect(self):
        """
        Connection using client API to GCS.
        """

        try:
            self.client = storage.Client(project=self.cred_project_id)
            self.gcs_bucket = self.client.bucket(self.bucket_name)
        except Exception as exc:
            self.logger.exception("\nGCS Client connection failed\n\n" + str(exc))
            raise xpr_exp.GCSConnectionFailed

    def import_data(self, bucket, source_file):
        """

        Imports a `data`frame from `source_file` in GCS `bucket`.

        Args:
            bucket (str): GCS bucket name
            source_file (str): file location in gcs bucket # strip /

        Returns:
             object: a pandas DataFrame.

        """
        self.connect()
        try:
            # TODO
            pass
        except Exception as exc:
            self.logger.exception("\nData import failed from GCS\n\n" + str(exc))
            raise xpr_exp.GCSConnectionFailed
        return None

    def write_object(self, source_file, destination_blob):
        """
        Writes a local file from `source_file` to GCS  bucket at `destination_blob`

        Args:
            source_file (str): local file path
            destination_blob (str): file location in gcs bucket
        """
        try:
            blob = self.gcs_bucket.blob(destination_blob)
            blob.upload_from_filename(source_file)
            print(f"File {source_file} uploaded to {destination_blob}.")
        except Exception as e:
            self.logger.error(f'Error when writing file to GCS\n\n {str(e)}')
            message = 'Failed to write to GCS bucket'
            raise xpr_exp.GCSConnectionFailed(message=message)

    def read_object(self, source_file, destination_file):
        """
        Reads a `source_file` in GCS bucket and writes to `destination_file` on
        local disk
        Args:
            source_file (str): file path on gcs bucket
            destination_file (str): file location on local disk
        """
        try:
            blob = self.gcs_bucket.get_blob(source_file)
            blob.download_to_filename(destination_file)
            print(f"File {source_file} downloaded to {destination_file}.")
        except Exception as e:
            self.logger.error(f'Error when writing file to GCS\n\n {str(e)}')
            message = 'Failed to read from GCS'
            raise xpr_exp.GCSConnectionFailed(message=message)

    def close(self):
        """
        Close GCS connection
        """

        return None
