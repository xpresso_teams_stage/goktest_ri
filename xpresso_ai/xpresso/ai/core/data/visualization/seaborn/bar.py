import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InputLengthMismatch
from xpresso.ai.core.commons.utils.constants import EMPTY_STRING, \
    BAR_CHART_TAIL_THRESHOLD
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.data.visualization.seaborn.res.plot_factory import Plot


class NewPlot(Plot):
    """
    Generates a bar plot
    Attributes:
        plot(:obj: matplotlib.axes): Matplotlib axes which stores the plot
    Args:
        input_1(list): data for x axis or data for base plot in case of
            target
        input_2(list): Y axis data when target not present
        target(list): target variable data
        plot_title(str): Title to the plot
        output_format (str): html or png plots to be generated
        file_name(str): File name of the plot to be stored
        axes_labels(dict): Dictionary of x_label, y_label and
        target_label
        output_path(str): path where the html/png plots to be stored
        threshold(:int): Threshold for clubbing extended tail bars
    """
    def __init__(self, input_1, input_2=None, target=None, plot_title=None,
                 output_format=utils.HTML,
                 file_name=None, axes_labels=None,
                 output_path=utils.DEFAULT_IMAGE_PATH, threshold=None):
        super().__init__()
        sns.set(font_scale=1.5)
        if axes_labels:
            self.axes_labels = axes_labels

        if target is None:
            self.base_plot(input_1, input_2, threshold)
        else:
            self.target_plot(input_1, target)
        plt.title(plot_title)
        self.render(output_format=output_format, output_path=output_path,
                    file_name=file_name)

    def base_plot(self, input_1, input_2, threshold=None):
        """
        Plots the base bar plot
        Args:
            input_1(:list): X axis data
            input_2(:list): Y axis data
            threshold(:int): Threshold for clubbing extended tail bars
        """
        if len(input_1) != len(input_2):
            raise InputLengthMismatch()

        # Process input data
        data = pd.DataFrame(list(zip(input_1, input_2)), columns=[0, 1])
        data = self.remove_tail(data, threshold=threshold)
        self.plot = sns.barplot(x=0, y=1, data=data, ci=None, orient='v')
        self.plot.set(xlabel=self.axes_labels[utils.X_LABEL],
                      ylabel=self.axes_labels[utils.Y_LABEL])
        self.optimize_xticks()

    def target_plot(self, input_1, target):
        """
        Plots the bar plot with target variable
        Args:
            input_1(:list): Data for base bar plot
            target(:list): Target variable data
        """
        data = pd.DataFrame(list(zip(input_1, target)), columns=[0, 1])
        new_data = pd.DataFrame(data[0].value_counts())
        new_data.reset_index(inplace=True)
        new_data.rename(columns={"index": 'category_1', 0: "total"},
                        inplace=True)

        data = pd.DataFrame(data.groupby([0, 1]).size(), columns=["count"])
        data.reset_index(inplace=True)
        data.rename(columns={0: 'category_1', 1: 'category_2'}, inplace=True)

        data = data.merge(new_data, on="category_1")
        data["count_percent"] = data["count"] / data["total"] * 100

        self.plot = sns.barplot(x="category_1", y="total", data=data, ci=None,
                                orient='v')
        self.plot.set(xlabel=self.axes_labels[utils.X_LABEL],
                      ylabel=self.axes_labels[utils.Y_LABEL])
        secondary_axis = plt.twinx()
        hue = 1
        if self.axes_labels[utils.TARGET_LABEL] != EMPTY_STRING:
            data.rename(
                columns={"category_2": self.axes_labels[utils.TARGET_LABEL]},
                inplace=True)
            hue = self.axes_labels[utils.TARGET_LABEL]
        self.plot = sns.catplot(ax=secondary_axis, x="category_1",
                                y="count_percent",
                                kind="point", data=data,
                                markers="o", hue=hue, ci=None)
        secondary_axis.set_ylabel(self.axes_labels[utils.TARGET_LABEL])
        secondary_axis.set_xlabel(self.axes_labels[utils.X_LABEL])
        secondary_axis.set(ylim=(0, 100))
        ylabels = ['{}'.format(x) + '%' for x in secondary_axis.get_yticks()]
        secondary_axis.set_yticklabels(ylabels)
        plt.close(2)

    def optimize_xticks(self):
        """ Optimizes number of xticks in bar plot"""
        self.plot.set_xticklabels(self.plot.xaxis.get_ticklabels(), rotation=45)
        xticks_labels = self.plot.xaxis.get_ticklabels()
        if len(xticks_labels) <= 20:
            step = 1
        else:
            step = int(len(xticks_labels) / (len(
                xticks_labels) - utils.DEFAULT_XTICKS_NUMBER))
        xticks_labels = list(set(xticks_labels) - set(xticks_labels[::step]))
        for label in xticks_labels:
            label.set_visible(False)

    @staticmethod
    def remove_tail(data, threshold=BAR_CHART_TAIL_THRESHOLD):
        """
            Removes the extended tails in bar charts and clubs small categories
        Args:
            data(:obj:Dataframe): Dataset for plotting.
            threshold(:int): Optional threshold of bar size to be clubbed.
        Returns:
            data(:obj:Dataframe): Dataset for plotting with extended tail
                removed
        """
        if not threshold and threshold != 0:
            threshold = BAR_CHART_TAIL_THRESHOLD
        max = data[1].max()

        less_than = {0: '', 1: 0}
        greater_than = {0: '', 1: 0}

        # Iterate through the data to club small size of bars at the beginning
        for index, value in data.iterrows():
            if value[1] / max * 100 > threshold:
                break
            less_than[1] += value[1]
            less_than[0] = "< {}".format(value[0])
            data.drop(inplace=True, index=index)

        # Iterate through the data to club small size of bars at the end
        temp_data = data.iloc[::-1]
        for index, value in temp_data.iterrows():
            if value[1] / max * 100 > threshold:
                break
            greater_than[1] += value[1]
            greater_than[0] = "> {}".format(value[0])
            data.drop(inplace=True, index=index)

        if less_than[1] > 0:
            data = data.append(less_than, ignore_index=True)
        if greater_than[1] > 0:
            data = data.append(greater_than, ignore_index=True)
        return data
